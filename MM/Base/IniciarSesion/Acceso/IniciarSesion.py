import pymysql
import config

"""
    @desc: Clase para manejar el login a ARMS 3
    @author: Erick Sandoval  

"""


class IniciarSesion:
    def __init__(self):
        """Constructor sin parametros"""
        self.db = pymysql.connect(host=config.database_data["dbHost"], user=config.database_data["dbUser"],
                                  passwd=config.database_data["dbPassword"], db=config.database_data["dbName"])
        pass
    """
        :return True si se pudo autenticar al usuario y settea el diccionario de config.usuario con los datos del usuario logueado
        :return False si no fue posible autenticar al usuario
    """
    def authenticate(self, user, password):
        """Metodo para consultar si el usuario con el usuario y password existen, el cedi puede no existir"""
        #SQL
        sql = "SELECT user.correo, user.Nombre, emp.Empresa, cedi.Cedi FROM usuarios user JOIN cedis cedi ON (user.idCedi = cedi.idCedi) JOIN empresas emp ON (cedi.idEmpresa = emp.IdEmpresa) WHERE user.correo = %s AND user.password = %s "
        sql = sql.format(user, password)
        #Obteniendo el cursor de la conexión a la base de datos
        with self.db.cursor() as cursor:
            cursor = self.db.cursor()
            cursor.execute(sql, (user, password))
            data = cursor.fetchone()
            #Si no se encuentra al usuario
            if data is None:
                config.usuario = {}
                config.session_started = False
                return False
            #Si si se encuentra al usuario
            else:
                #
                config.usuario = {
                    "correo":data[0],
                    "nombre":data[1],
                    "empresa":data[2],
                    "cedi":data[3]

                }
                config.session_started = True
                print (config.usuario)
                return True


# if __name__ == "__main__":
#     iss = IniciarSesion()
#     print(iss.authenticate('avazquez@arete.ws', 'avazquez'))
#     pass
