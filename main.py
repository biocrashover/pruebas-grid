import  sys
from    PyQt5.QtWidgets import QWidget, QApplication, QMainWindow, QTableWidget, QTableWidgetItem,QAction,QVBoxLayout
from    PyQt5 import uic
from PyQt5.QtCore import pyqtSlot
import json
from datetime import datetime
import requests
# from    mainwindow import *

class Ventana(QMainWindow):
    def __init__(self):
        # funcion llamada ApiConnect

        #iniciar QMainWindow
        QMainWindow.__init__(self)
        uic.loadUi("mainwindow.ui",self)

        self.setWindowTitle("Titulo de la ventana")
        row=0
        self.Texto1.setText("hola")
        self.tableWidget.setColumnCount(3)
        self.tableWidget.setRowCount(3)
        self.tableWidget.setHorizontalHeaderLabels(['Viaje','Placas','Km'])
        self.tableWidget.insertRow(row)
        data=self.viajesDespegados()
        viajes=json.loads(data['Data'])
        self.tableWidget.setRowCount(len(viajes['viajes']))
        for viaje in viajes['viajes']:
            self.tableWidget.setItem(row,0, QTableWidgetItem(str(viaje['Viaje'])))
            self.tableWidget.setItem(row,1, QTableWidgetItem(viaje["Placas"]))
            self.tableWidget.setItem(row,2, QTableWidgetItem(str(viaje["Km"])))
            # self.tableWidget.setItem(row,2, QTableWidgetItem(datos["apellidop"]))
            # self.tableWidget.setItem(row,3, QTableWidgetItem(datos["apellidom"]))
            row=row+1
            print(viaje['Viaje'])
        # self.tableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tableWidget.clicked.connect(self.on_click)

    @pyqtSlot()
    def on_click(self):
        print("\n")
        for currentQTableWidgetItem in self.tableWidget.selectedItems():
            print(currentQTableWidgetItem.row(), currentQTableWidgetItem.column(), currentQTableWidgetItem.text())


    def viajesDespegados(self):
        print('** obteniendo viajes asignados:' + str(datetime.now()))
        headers = {'content-type': 'application/json','X-API-KEY':'mtS5hV3rl53tBeGeWnfWq2pqxSRDjOvJW6L7hUb9'}
        url = "https://b2uiut18x9.execute-api.us-east-1.amazonaws.com/dev/obtener-viajes"
        datos = {"empresaId": 10,"status": 8.5,"cedi":"POCHTECA CEDI 1"}
        try:
            r = requests.post(url, data=json.dumps(datos),headers=headers).json()
            print(r.keys())
            # respuesta=json.loads(r)
            # json.loads(r)
            # print(r.json())
        except requests.exceptions.HTTPError as err:
            print("fallo")
            print(err)
        return r

app = QApplication(sys.argv)
# ARMS = QtWidgets.QMainWindow()
_ventana = Ventana()
# ui.setupUi(ARMS)
_ventana.show()
app.exec_()
