import sys
import os

class AccionesBotones:
    def __init__(self, ui):
        self.ui = ui

    def btnAbreSSResultados(self):
        try:
            # ---------------------------------------------------------------------
            # datos del SS Resultados
            self.SSResultados = self.ui.comboBoxSSResultados.currentText()
            if self.ui.radioSSUrl.isChecked():
                self.SSResultadosTipoArchivo = 'url'
            if self.ui.radioSSKey.isChecked():
                self.SSResultadosTipoArchivo = 'key'
            if self.ui.radioSSNombre.isChecked():
                self.SSResultadosTipoArchivo = 'nombre'
            # ---------------------------------------------------------------------
            # abre el SS Resultados
            ok, duracion, config.ROriginal = ImportarDatos.importaGoogleSS.cargaResultados(self.SSResultados, 'RESULTADOS', tipo_nombre_ss=self.SSResultadosTipoArchivo, abierto=False)
            # abre el SS Compatibilidad
            ok, duracion, config.C = ImportarDatos.importaGoogleSS.cargaResultados(self.SSResultados, 'COMPATIBILIDAD', tipo_nombre_ss=self.SSResultadosTipoArchivo, abierto=True)
            # ---------------------------------------------------------------------
            # datos de los Arcos
            arcos = self.ui.comboBoxArcos.currentText()
            if self.ui.radioArcosLocal.isChecked():
                self.ArcosTipoArchivo = 'local'
            if self.ui.radioArcosS3.isChecked():
                self.ArcosTipoArchivo = 's3'
            # ---------------------------------------------------------------------

            # todo falta importa arcos
            # ---------------------------------------------------------------------
            if ok:
                self.ui.lblAbreSSResultados.setText('Abierto')
            else:
                self.ui.lblAbreSSResultados.setText(duracion)
            # ---------------------------------------------------------------------
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name,this_function_name,str(e))
            print(error)
