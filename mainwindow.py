# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setEnabled(True)
        MainWindow.resize(803, 553)
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.centralWidget.setObjectName("centralWidget")
        self.tableViewPrfifo = QtWidgets.QTableView(self.centralWidget)
        self.tableViewPrfifo.setGeometry(QtCore.QRect(10, 50, 781, 192))
        self.tableViewPrfifo.setObjectName("tableViewPrfifo")
        self.Texto1 = QtWidgets.QLabel(self.centralWidget)
        self.Texto1.setGeometry(QtCore.QRect(650, 10, 131, 31))
        self.Texto1.setObjectName("Texto1")
        self.Texto1_2 = QtWidgets.QLabel(self.centralWidget)
        self.Texto1_2.setGeometry(QtCore.QRect(660, 250, 131, 31))
        self.Texto1_2.setObjectName("Texto1_2")
        self.tableWidget = QtWidgets.QTableWidget(self.centralWidget)
        self.tableWidget.setGeometry(QtCore.QRect(10, 290, 781, 192))
        self.tableWidget.setStyleSheet("border:1px solid #4d4d4d;\n"
"background-color:#dddddd;\n")
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.pushButton = QtWidgets.QPushButton(self.centralWidget)
        self.pushButton.setGeometry(QtCore.QRect(10, 10, 113, 32))
        self.pushButton.setObjectName("pushButton")
        MainWindow.setCentralWidget(self.centralWidget)
        self.menuBar = QtWidgets.QMenuBar(MainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 803, 22))
        self.menuBar.setObjectName("menuBar")
        MainWindow.setMenuBar(self.menuBar)
        self.mainToolBar = QtWidgets.QToolBar(MainWindow)
        self.mainToolBar.setObjectName("mainToolBar")
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.mainToolBar)
        self.statusBar = QtWidgets.QStatusBar(MainWindow)
        self.statusBar.setObjectName("statusBar")
        MainWindow.setStatusBar(self.statusBar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.Texto1.setText(_translate("MainWindow", "Tabla desde modelo"))
        self.Texto1_2.setText(_translate("MainWindow", "Tabla desde item"))
        self.pushButton.setText(_translate("MainWindow", "Cambiar"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
