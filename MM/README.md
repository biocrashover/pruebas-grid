# ARMSApp

** Documentación de la app **

---------------------------------

## Indice

---------------------------------

* Indice
* Requerimientos
* Ejecutar
* Enlazar botones
* Reglas generales
* Bonus


## Requerimientos

---------------------------------

* Python 3

        https://www.python.org/downloads/

* Environment

        pip install virtualenv

* Xcode (sólo en Mac)

        https://itunes.apple.com/mx/app/xcode/id497799835?mt=12
    
* PyQt5

	    pip3 install pyqt5
    
* Pandas

	    pip3 install pandas
    
* PygSheets

	    pip3 install pygsheets
    
* Requests

	    pip3 install requests
    
* PyMysql

	    pip3 install pymysql

## Ejecutar

---------------------------------

* En la carpeta del proyecto para activar el entorno virtual:
    
        source env/bin/activate

* Se desactiva el entorno virtual con:
    
        deactivate

* Corre en la carpeta del proyecto con:

	    python main.py
	
## Enlazar botones

---------------------------------

* __main.py__

Archivo principal que genera la vista, aquí se van a enlazar todos los botones con su manejador.

_Conectar un botón con su manejador:_

ui.<Nombre del botón>.clicked.connect(<Instancia del manejador>.<Método del manejador>)

        ui.pushButton.clicked.connect(a_btn.abrirSS)

* __arms_signals.py__

Archivo manejador de eventos para todos los botones.

_Ejemplo de método manejador:_

        def abrirSS(self):
            try:
                spreadsheet = SpreadSheet()
                ok, duracion, resultado = spreadsheet.AbrirSS("1TddiMNgdUPIv5ppEUDbotKTxkmeQ_BS9bRAs_TO2GfM", "VEHICULOS", "VEHICULOS")

                if ok:
                    print (duracion)
                    print (resultado)
                else:
                    print ("Hubo un error")
            except Exception as e:
                file_name = os.path.basename(sys.argv[0])
                this_function_name = sys._getframe().f_code.co_name
                error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
                print(error)
 
_Las clases individuales se manejan como de costumbre_
	
## Reglas generales

---------------------------------

* Todos los métodos deben tener try/except con el siguiente formato:

        try:
            print ("Ejemplo")
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
            return False, error, False

* Todos los métodos deben regresar 3 parámetros ok, duracion, resultado:

        start_time = datetime.now()
        try:
            resultado = "Ejemplo"
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
            return False, error, False
        duracion = datetime.now() - start_time
        return True, duracion, resultado

## Bonus

---------------------------------

* Generar archivo Requirements.txt con versión de librerias fija

        pip freeze > requirements.txt

* Instalas todas las librerías necesarias de un jalón

        pip install -r Requirements.txt
