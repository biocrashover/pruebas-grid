# -*- coding: utf-8 -*-
import requests
import json
from datetime import datetime

class SpreadSheet:
    
    def AbrirSS(self, id, sheetname, tipo):
        start_time = datetime.now()
        try:
            url = 'https://b2uiut18x9.execute-api.us-east-1.amazonaws.com/dev/leer-spreadsheet'
            payload = {"spreadsheetId": id, "sheetName": sheetname, "tipo": tipo}
            headers = {'content-type': 'application/json', 'X-API-KEY': 'mtS5hV3rl53tBeGeWnfWq2pqxSRDjOvJW6L7hUb9'}

            r = requests.post(url, data=json.dumps(payload), headers=headers)
            resultado = r.text

        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name,this_function_name,str(e))
            return False, error, False
        duracion = datetime.now() - start_time
        return True, duracion, resultado