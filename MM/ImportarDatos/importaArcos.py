import config
import sys
import os
from datetime import datetime
import boto3
import requests
import json
from io import StringIO
import pandas as pd

class ApiConnect():

    @staticmethod
    def arcos(payload):
        print ('** ejecutando...', str(datetime.now()),  'arcos')
        headers = {'content-type': 'application/json'}
        url = "http://ec2-54-91-123-254.compute-1.amazonaws.com/api/arcos/"
        try:
            r = requests.post(url, data=json.dumps(payload),headers=headers)
            r.raise_for_status()
        except requests.exceptions.HTTPError as err:
            print("fallo")
            print(err)
        return r

    @staticmethod
    def destinosUnicos(payload):
        print ('** ejecutando...', str(datetime.now()),  'destinosUnicos')
        headers = {'content-type': 'application/json'}
        url = "https://b2uiut18x9.execute-api.us-east-1.amazonaws.com/dev/destinos-arcos"
        try:
            r = requests.post(url, data=json.dumps(payload),headers=headers)
            r.raise_for_status()
        except requests.exceptions.HTTPError as err:
            print("fallo")
            print(err)
        return r

    @staticmethod
    def archivoS3(empresa,nombre):
        print ('** ejecutando...', str(datetime.now()),  'archivoS3')

        bucket,key,secret = ApiConnect.config()
        s3 = boto3.resource('s3',
            aws_access_key_id=key,
            aws_secret_access_key=secret
            )
        prefix = empresa+'/'+nombre+'/arcos.txt'
        o = s3.Object(bucket,prefix)
        try:
            o.load()
            body = o.get()['Body'].read().decode('utf-8')
            return body
        except Exception:
            return None

    @staticmethod
    def config():
        print ('** ejecutando...', str(datetime.now()),  'config')
        env = "dev"
        if env == "prod":
            bucket ="arete.arms3.arcos.prod"
        elif env =="beta":
            bucket ="arete.arms3.arcos.beta"
        elif env =="dev":
            bucket ="arete.arms3.arcos.dev"
        return bucket,"AKIAIGPFZH4MAPGTNCGQ","jzL4NBcqMNTedPChg0CaytfIk5vu2yHaHKZM6PHQ"

    def archivoArcosS3(self, w):
        print ('** ejecutando...', str(datetime.now()),  'archivoArcosS3')
        try:
            if w.arcos_tipo == False:
                archivo = ApiConnect.archivoS3(w.empresa, w.arcos)
                if archivo is not None:
                    config.A = pd.read_csv(StringIO(archivo), sep='\t',
                                         names=['ORIGEN', 'DESTINO', 'DISTANCIA', 'TIEMPO', 'LATITUD_DESTINO_A',
                                                'LONGITUD_DESTINO_A', 'LATITUD_DESTINO_B', 'LONGITUD_DESTINO_B',
                                                'FECHA_CALCULO'])
                else:
                    self.b('No existe Archivo Arcos. Empresa:{} Arcos:{}'.format(w.empresa, w.arcos), '**** Error ****')
            else: # es local
                config.A = pd.read_csv(w.arcos, sep='\t',
                                     names=['ORIGEN', 'DESTINO', 'DISTANCIA', 'TIEMPO', 'LATITUD_DESTINO_A',
                                            'LONGITUD_DESTINO_A', 'LATITUD_DESTINO_B', 'LONGITUD_DESTINO_B',
                                            'FECHA_CALCULO'])
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, this_function_name, str(e))
            print(error)

