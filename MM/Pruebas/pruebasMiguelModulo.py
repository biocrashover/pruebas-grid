# ---------------------------------------------------------------------
#provisional para ver los nombre!!!!
# Qt windows creadas en Qt Creator
from Qt.arms import *
# ---------------------------------------------------------------------



import config

from datetime import datetime
from datetime import timedelta
import numpy as np
import pandas as pd
import json


def optimizaAutomatico():
    # crear una instancia de mi clase Optimiza
    o = Optimiza(config.mainWindow)

    vacio = int(config.mainWindow.lineEditGeneradorOptimizaViajesVacios.text())
    cercania = int(config.mainWindow.lineEditOptimizaCercaniaMaxima.text()) * 1000
    valor = int(config.mainWindow.lineEditOptimizaAumentarValorMiles.text())*1000
    ventana =  int(config.mainWindow.lineEditOptimizaAumentarVentanaMinutos.text())
    volumen = 1 + (config.mainWindow.lineEditOptimizaAumentarCapacidad.text())
    peso = 1 + int(config.mainWindow.lineEditOptimizaAumentarPeso.text())
    ignoraAcceso = config.mainWindow.checkBoxOptimizaIgnoraAcceso
    ignoraMatriz = config.mainWindow.checkBoxOptimizaIgnoraCompatibilidad

    # ejecuta vacios o cercanos
    if config.mainWindow.radioButtonOptimizaCercanos.isChecked(vacio, cercania, valor, ventana, volumen, peso):
        #o.optimiza_cercanos(config.mainWindow)
        print('cer')

    else:
        #o.optimiza_vacios(config.mainWindow)
        print('vac')


    print(config.mainWindow.labelOptimizaAvance.text())

    return True, duracion, 0



class Optimiza:

    def __init__(self, ui):
        print ('** ejecutando...', str(datetime.now()),  'ApiConnect')

        #---------------------------------------------------
        self.t = datetime.now()
        self.contBitacora = 0
        self.CalculosTableroOriginal = None
        self.CalculosTableroOptimizado = None
        self.gc = None
        self.bitacora = pd.DataFrame(data=None, columns=['Evento'])
        self.contador = 0

    def b(self, s1, s2):
        self.contador += 1
        if len(str(s2).strip()) > 0:
            s = str(s1).strip() + '... [' + str(s2).strip() + ']'
        else:
            s = str(s1).strip()
        self.bitacora.loc[self.contador] = [s]

    def rutina_inicial(self, ui):
        print('** ejecutando...', str(datetime.now()),  'rutina_inicial')
        try:
            # ---------------------------------------------------
            self.R = config.dfResultados
            # ---------------------------------------------------
            # bitacora
            self.contBitacora = 0
            self.bitacora = pd.DataFrame(data=None, columns=['Evento'])
            self.contBitacora_cambios = 0
            # ---------------------------------------------------
            # guarda y luego vuelve a meterlos los PEDIDOS Dedicados = TRUE
            self.RDedicados = self.ROriginal.loc[(self.ROriginal.Dedicado == 'Si'), :].copy()
            self.R = self.ROriginal.loc[(self.ROriginal.Dedicado == 'No'), :].copy()
            self.b('Se eliminaron Pedidos Dedicados (diferente de No), ', '...ya que no se pueden combinar con otros viajes')
            # ---------------------------------------------------
            if self.valida_datos() == False:
                return False
            # ----------------------------------------------------
            # multiplica los valores por parametros deseados
            self.R.VolumenMax *=  ((100+ui.volumen) / 100)
            self.R.PesoMax *=  ((100+ui.peso) / 100)
            self.R.ValorMax += ui.valor*1000
            # ventanas
            if ui.ventana > 0:
                for index, row in self.R.iterrows():
                    self.R.loc[index, 'VentanaHoraFinPedido'] = roui.VentanaHoraFinPedido + timedelta(minutes = ui.ventana)
            # ----------------------------------------------------
            # todo: falta enviar los datos al THREAD
            TableroOriginal = self.tablero(config.dfResultados)
            # ----------------------------------------------------

            print('TERMINE Rutina Inicial', str(datetime.now() - self.t))
        # ----------------------------------------------------
        except Exception as e:
            print('optimiza.py - def rutina_inicial', e)
            self.errores(e, '*** error *** def rutina_inicial *** except Exception ***')
            return False

    def rutina_final(self, ui, REGRESA):
        print ('** ejecutando...', str(datetime.now()),  'rutina_final')

        try:
            # ***************************************************
            # todo lo que hay que hacer al terminar y regresar "REGRESA"
            # ***************************************************
            print ('Inicio Rutina Final', str(datetime.now()- self.t))

            # ----------------------------------------------------
            # Regresa lo inflado de parametros a su lugar
            # DIVIDE los valores por parametros deseados
            REGRESA.VolumenMax /= (100+ui.volumen) / 100
            REGRESA.PesoMax /= (100+ui.peso / 100)
            REGRESA.ValorMax -=  ui.valor * 1000
            # ventanas
            if ui.ventana > 0:
                for index, row in REGRESA.iterrows():
                    self.R.loc[index, 'VentanaHoraFinPedido'] = roui.VentanaHoraFinPedido - timedelta(minutes = ui.ventana)
            # ----------------------------------------------------

            # ----------------------------------------------------
            # borra las ultimas distancias
            if 'DISTANCIA' in REGRESA.columns:
                REGRESA = REGRESA.drop(['DISTANCIA', 'TIEMPO'], axis=1)
            # ----------------------------------------------------

            # ----------------------------------------------------
            # recalcula como quedo la ocupacion y grabalo en REGRESO
            v_sum = REGRESA.groupby(REGRESA.index).sum()
            v_max = REGRESA.groupby(REGRESA.index).max()

            REGRESA.OcupacionVolumen = v_sum.Volumen / v_max.VolumenMax * 100
            REGRESA.OcupacionPeso = v_sum.Peso / v_max.PesoMax * 100
            # ----------------------------------------------------

            #----------------------------------------------------
            self.b('*****************************************************', '')
            self.b('FINALIZA OPTIMIZACION', datetime.now())

            # ----------------------------------------------------
            #  regresa fechas y horas a como estaban
            self.transforma_fechas(REGRESA)
            # ----------------------------------------------------
            #cacatena los registros que eliminamos que venian como DEDICADOS = Si...son parte del plan
            if not self.RDedicados.empty:
                REGRESA = pd.concat([REGRESA, self.RDedicados])
                self.b('*****************************************************', '')
                self.b('Registros Dedicados', self.RDedicados.shape[0])
            # ----------------------------------------------
            # tablero
            self.b('TABLERO - SIN OPTIMIZAR', self.CalculosTableroOriginal)
            self.b('TABLERO - OPTIMIZADO', self.CalculosTableroOptimizado)
            # ----------------------------------------------

            self.b('*****************************************************', '')
            self.b('********** DESTINOS REUBICADOS --->', self.contBitacora_cambios)
            self.b('*****************************************************', '')
            #----------------------------------------------------
            print ('TERMINE Rutina Final', str(datetime.now()- self.t))

            return REGRESA
        # ----------------------------------------------------
        except Exception as e:
            print('optimiza.py - def rutina_final', e)
            self.errores(e, 'CERCANOS.PY - def rutina_final *** except Exception ***')
            return False

    def valida_datos(self):
        try:
            print('** ejecutando...', str(datetime.now()),  'valida_datos')
            # ----------------------------------------------------
            # validaciones de que lleguen los datos bien R, C y A
            # ----------------------------------------------------
            # checa que no traiga nulls o Nan
            if (self.R.isnull().any().any()) == True:
                print('ERROR....hay NaN o nulls en Resultados')
                self.b('ERROR....hay NaN o nulls en Resultados', datetime.now())
                return False
            # checa que no traiga nulls o Nan
            if (self.C.isnull().any().any()) == True:
                print('ERROR....hay NaN o nulls en matriz de Complementos')
                self.b('ERROR....hay NaN o nulls en matriz de Complementos', datetime.now())
                return False
            # checa que no traiga nulls o Nan
            if (self.A.isnull().any().any()) == True:
                print('ERROR....hay NaN o nulls en Arcos')
                self.b('ERROR....hay NaN o nulls en Arcos', datetime.now())
                return False
            # ----------------------------------------------------

            # ----------------------------------------------------
            # quitale a todos los espacio, antes y despues al nombre de columnas
            self.R.columns.str.strip()
            self.C.columns.str.strip()
            self.A.columns.str.strip()
            # ----------------------------------------------------


            # ----------------------------------------------------
            # verifica que lleguen los nombres de las columnas que necesito
            if not {'DestinoTR1', 'Volumen', 'Peso', 'Valor', 'Piezas', 'FechaEntregaPedido', 'FechaSalidaPedido',
                    'TiemDescarga', 'Productos', 'VentanaFechaInicioPedido', 'VentanaFechaFinPedido', 'VentanaHoraInicioPedido',
                    'VentanaHoraFinPedido', 'RestriccionVolumen', 'Viaje', 'Tirada', 'TipoVehiculo', 'VolumenMax','PesoMax', 'ValorMax',
                    'OcupacionVolumen', 'OcupacionPeso', 'Dedicado', 'FechaSalida', 'FechaRetorno', 'Km', 'Co2', 'CostoTotal',
                    'ModificadoVacios', 'ModificadoCercanos', 'FechaEntregaTR2', 'FechaEjecucionOpti', 'DuracionViaje' }.issubset(self.R.columns):
                self.b('Nombre de campo incorrecto', 'DestinoTR1, Volumen, Peso, Valor, Piezas, FechaEntregaPedido, FechaSalidaPedido, TiemDescarga, Productos, VentanaFechaInicioPedido, VentanaFechaFinPedido, VentanaHoraInicioPedido, VentanaHoraFinPedido, RestriccionVolumen, Viaje, Tirada, TipoVehiculo, VolumenMax, PesoMax, ValorMax, OcupacionVolumen, OcupacionPeso, Dedicado, FechaSalida, FechaRetorno, Km, Co2, CostoTotal,ModificadoVacios, ModificadoCercanos, FechaEntregaTR2, FechaEjecucionOpti')
                return False
            # ----------------------------------------------------

            # ----------------------------------------------------
            # cambia las columnas a tipo NUMERIC
            self.R[['Volumen', 'Peso', 'Valor', 'Piezas', 'TiemDescarga']] = self.R[['Volumen', 'Peso', 'Valor', 'Piezas', 'TiemDescarga']].apply(pd.to_numeric)
            self.R[['Tirada', 'VolumenMax','PesoMax', 'ValorMax']] = self.R[['Tirada', 'VolumenMax','PesoMax', 'ValorMax']].apply(pd.to_numeric)
            self.R[['RestriccionVolumen', 'OcupacionVolumen', 'OcupacionPeso', 'Km', 'Co2', 'CostoTotal']] = \
            self.R[['RestriccionVolumen', 'OcupacionVolumen', 'OcupacionPeso', 'Km', 'Co2', 'CostoTotal']].apply(pd.to_numeric)

            # cambia las columnas a tipo  DATETIME
            self.R['FechaSalida'] = pd.to_datetime(self.R['FechaSalida'], dayfirst=True)
            self.R['FechaRetorno'] = pd.to_datetime(self.R['FechaRetorno'], dayfirst=True)
            self.R['FechaEntregaPedido'] = pd.to_datetime(self.R['FechaEntregaPedido'], dayfirst=True)
            self.R['FechaSalidaPedido'] = pd.to_datetime(self.R['FechaSalidaPedido'], dayfirst=True)
            self.R['VentanaFechaInicioPedido'] = pd.to_datetime(self.R['VentanaFechaInicioPedido'], dayfirst=True)
            self.R['VentanaFechaFinPedido'] = pd.to_datetime(self.R['VentanaFechaFinPedido'], dayfirst=True)
            self.R['VentanaHoraInicioPedido'] = pd.to_datetime(self.R['VentanaHoraInicioPedido'], dayfirst=True)
            self.R['VentanaHoraFinPedido'] = pd.to_datetime(self.R['VentanaHoraFinPedido'], dayfirst=True)

            self.R['FechaEntregaTR2'] = pd.to_datetime(self.R['FechaEntregaTR2'], dayfirst=True)
            self.R['FechaEjecucionOpti'] = pd.to_datetime(self.R['FechaEjecucionOpti'], dayfirst=True)

            # ----------------------------------------------------

            return True

            # ----------------------------------------------------
        except Exception as e:
            self.errores(e, '*** error *** def valida_datos *** except Exception ***')
            return False

    def transforma_fechas(self, df):
        print ('** ejecutando...', str(datetime.now()),  'transforma_fechas')

        # -----------------------------------------------------
        # fechas a dd/mm/aaaa
        df.FechaEntregaPedido = df.FechaEntregaPedido.dt.strftime('%m/%d/%Y')
        df.FechaSalidaPedido = df.FechaSalidaPedido.dt.strftime('%m/%d/%Y')
        df.VentanaFechaInicioPedido = df.VentanaFechaInicioPedido.dt.strftime('%m/%d/%Y')
        df.VentanaFechaFinPedido = df.VentanaFechaFinPedido.dt.strftime('%m/%d/%Y')
        df.FechaSalida = df.FechaSalida.dt.strftime('%m/%d/%Y')
        df.FechaRetorno = df.FechaRetorno.dt.strftime('%m/%d/%Y')

        df.FechaEntregaTR2 = df.FechaEntregaTR2.dt.strftime('%m/%d/%Y')
        df.FechaEjecucionOpti = df.FechaEjecucionOpti.dt.strftime('%m/%d/%Y')

        # -----------------------------------------------------

        # -----------------------------------------------------
        # horas
        df.VentanaHoraInicioPedido = df.VentanaHoraInicioPedido.dt.strftime('%H:%M')
        df.VentanaHoraFinPedido = df.VentanaHoraFinPedido.dt.strftime('%H:%M')

        # -----------------------------------------------------

        df.OcupacionVolumen = df.OcupacionVolumen.round(0)
        df.OcupacionPeso = df.OcupacionPeso.round(0)

    def restricc_acceso(self, ui, df, viaje, destino, viaje_quiero_meter):
        """ verifica si cabe Restrccion de Acceso """
        print ('** ejecutando...', str(datetime.now()),  'restricc_acceso')
        try:
            if ui.ignoraAcceso == True:
                return True

            d = df.loc[(df.Viaje == viaje) & (df.DestinoTR1 == destino)].RestriccionVolumen.unique()
            v = df.loc[df.Viaje == viaje_quiero_meter].RestriccionVolumen.unique()

            # por si meten error y ponen mas de dos volumenes por camion
            d = d[0]
            v = v[0]



            if d >= v:
                return True
            else:
                x = 'Destino acepta MAX. {} m3, viaje tiene {} m3'.format(d, v)
                self.b('NO CUMPLE RESTRICCION Acceso, ', x)
                return False
        # ----------------------------------------------------
        except Exception as e:
            print('optimiza.py - def restricc_acceso', e)
            self.errores(e, '*** error *** def restricc_acceso *** except Exception ***')
            return False

    def restricc_volumen(self, df, viaje, destino, viaje_quiero_meter):
        """ verifica si cabe  Vol, Peso y Valor """
        print ('** ejecutando...', str(datetime.now()),  'restricc_volumen')
        try:

            d = df.loc[(df.Viaje == viaje) & (df.DestinoTR1 == destino)]
            v = df.loc[df.Viaje == viaje_quiero_meter]

            #  ------- obten los totales para trabajar
            # totales del viaje
            viaje_vol = v['Volumen'].sum()
            # maximos del viaje
            viaje_volmax = v['VolumenMax'].max()
            # totales del destino que quiero meter
            destino_volumen = d['Volumen'].sum()

            # Suma y dime si se pasa o le cabe (negativo s pasa)
            sobra_viaje_volumen = viaje_volmax - viaje_vol - destino_volumen
            # volumen
            if sobra_viaje_volumen < 0.0:
                self.b('NO CUMPLE RESTRICCION Volumen', 'Sobra Volumen: ' + str(sobra_viaje_volumen) + ' = ' +
                     ' Vol Max: ' + str(viaje_volmax) + ' -  en viaje: ' + str(
                    viaje_vol) + ' -  nuevo destino: ' + str(destino_volumen))
                return False

            return True
        # ----------------------------------------------------
        except Exception as e:
            print('optimiza.py - def restricc_volumen', e)
            self.errores(e, '*** error *** def restricc_volumen *** except Exception ***')
            return False

    def restricc_peso(self, df, viaje, destino, viaje_quiero_meter):
        """ verifica si cabe  Vol, Peso y Valor """
        print ('** ejecutando...', str(datetime.now()),  'restricc_peso')
        try:
            d = df.loc[(df.Viaje == viaje) & (df.DestinoTR1 == destino)]
            v = df.loc[df.Viaje == viaje_quiero_meter]

            #  ------- obten los totales para trabajar
            # totales del viaje
            viaje_peso = v['Peso'].sum()
            # maximos del viaje
            viaje_pesomax = v['PesoMax'].max()
            # totales del destino que quiero meter
            destino_peso = d['Peso'].sum()

            # Suma y dime si se pasa o le cabe (negativo s pasa)
            # Peso
            sobra_viaje_peso = viaje_pesomax - viaje_peso - destino_peso
            if sobra_viaje_peso < 0:
                self.b('NO CUMPLE RESTRICCION Peso', 'Sobra Peso: ' + str(sobra_viaje_peso) + ' = ' +
                     ' (peso.max) ' + str(viaje_pesomax) + ' - (trae viaje) ' + str(
                    viaje_peso) + ' -  (destino a meter) ' + str(destino_peso))
                return False

            return True
        # ----------------------------------------------------
        except Exception as e:
            print('optimiza.py - def restricc_peso', e)
            self.errores(e, '*** error *** def restricc_peso *** except Exception ***')
            return False

    def restricc_valor(self, df, viaje, destino, viaje_quiero_meter):
        """ verifica si cabe  Vol, Peso y Valor """
        print ('** ejecutando...', str(datetime.now()),  'restricc_valor')
        try:

            d = df.loc[(df.Viaje == viaje) & (df.DestinoTR1 == destino)]
            v = df.loc[df.Viaje == viaje_quiero_meter]

            #  ------- obten los totales para trabajar
            # totales del viaje
            viaje_valor = v['Valor'].sum()
            # maximos del viaje
            viaje_valormax = v['ValorMax'].max()
            # totales del destino que quiero meter
            destino_valor = d['Valor'].sum()

            # Suma y dime si se pasa o le cabe (negativo s pasa)
            # Valor
            sobra_viaje_valor = viaje_valormax - viaje_valor - destino_valor
            if sobra_viaje_valor < 0:
                self.b('NO CUMPLE RESTRICCION Peso', 'Sobra Peso: ' + str(sobra_viaje_valor) + ' = ' +
                     ' (valor.max) ' + str(viaje_valormax) + ' - (trae viaje) ' + str(
                    viaje_valor) + ' -  (destino a meter) ' + str(destino_valor))
                return False

            return True
        # ----------------------------------------------------
        except Exception as e:
            print('optimiza.py - def restricc_valor', e)
            self.errores(e, '*** error *** def restricc_valor *** except Exception ***')
            return False

    def restricc_matriz_comp(self, ui, df, viaje, destino, viaje_quiero_meter):
        """ verifica que el listado de productos del destino, sea compatible con el listado de productos del viaje """
        print ('** ejecutando...', str(datetime.now()),  'restricc_matriz_comp')
        try:
            if ui.ignoraMatriz == 1:
                return True

            d = df.loc[(df.Viaje == viaje) & (df.DestinoTR1 == destino)]
            v = df.loc[df.Viaje == viaje_quiero_meter]

            # convierte los productos a listados
            listado_productos_viaje = self.productos_a_listado(v)
            listado_productos_destino = self.productos_a_listado(d)
            # has el match entre todas las parejas del camion y dle destino
            for i in listado_productos_destino:
                for j in listado_productos_viaje:
                    valor = self.C.loc[(self.C.ClaveProducto2 == int(i)) & (self.C.ClaveProducto1 == int(j)), 'ValorCompatibilidad']
                    if valor.values == 'NO':
                        # AQUI PORQUE NO ENTRO....[i, j, valor.values]
                        self.b(' NO CUMPLE RESTRICCION Matriz Compatibilidad',
                             'Producto: ' + str(i) + ' con Producto ' + str(j))
                        return False

            return True
        # ----------------------------------------------------
        except Exception as e:
            print('optimiza.py - def restricc_matriz_comp', e)
            self.errores(e, '*** error *** def restricc_matriz_comp *** except Exception ***')
            return False

    def restricc_ventanas(self, df, pedidos_destino, pedidos_viaje, viaje_quiero_meter):
        """ verifica si es posible entregar el destino en tiempo en el viaje """
        print ('** ejecutando...', str(datetime.now()),  'restricc_ventanas')

        try:
            v = pedidos_destino.iloc[0].Viaje
            d = pedidos_destino.iloc[0].DestinoTR1

            # me traigo los horarios de destino y viaje
            horario_d = self.ventana_destino(pedidos_destino)
            horario_v = self.ventana_viaje(pedidos_viaje)

            # este factor es provisional, es posible que cambie con DEEP Learning...aplico el MEAN a las distancias del viaje
            factor_google = 1.5
            tiempo_destino_min = df.loc[df.Viaje == viaje_quiero_meter].TIEMPO.mean() / 60 * factor_google

            if not (tiempo_destino_min > 0):
                self.b(' NO CUMPLE RESTRICCION Ventana a Tiempo NAN', 'tiempo_destino_min')
                return False, None, None

            # busca la fecha en viaje
            tirada = 0
            for key in horario_d:
                if key in horario_v.keys():  # encontre un dia que deseo entregar dentro de los dias del Viaje
                    # trae su datetime
                    x = horario_v[key]
                    # separa el datetime de la tirada
                    fecha = x[0]
                    tirada = x[1]
                    # obten el horario maximo a entregar segun el DESTINO
                    horario_max_para_entregar = horario_d[key].time()

                    # suma a la hora ultima del viaje....el traslado segun google (es promedio de los destinos que trae viaje!!)
                    suma_traslado_y_ultima_hora = fecha + timedelta(minutes=tiempo_destino_min)
                    tiempo_final = suma_traslado_y_ultima_hora.time()

                    if tiempo_final <= horario_max_para_entregar:
                        dia_con_hora_entrega = datetime.combine(fecha.date(), tiempo_final)
                        return True, dia_con_hora_entrega, tirada + 1
                    else:
                        # AQUI PORQUE NO ENTRO....
                        dia_con_hora_entrega = datetime.combine(key, tiempo_final)
                        self.b('NO CUMPLE RESTRICCION Ventana {}. Max hora: {}'.format(dia_con_hora_entrega, horario_max_para_entregar), '')
                else:
                    # metelo en otro dia a las 8 am
                    t = datetime.strptime('08:00', '%H:%M').time()
                    dia_con_hora_entrega = datetime.combine(key, t)
                    self.b(' ...SI CUMPLE OTRO DIA, : {}'.format(dia_con_hora_entrega), '')
                    return True, dia_con_hora_entrega, tirada + 1

            self.b(' NO CUMPLE RESTRICCION Ventana', 'Sin Horario')
            return False, None, None
        # ----------------------------------------------------
        except Exception as e:
            print('optimiza.py - def restricc_ventanas', e)
            self.errores(e, '*** error *** def restricc_ventanas *** except Exception ***')
            return False, None, None

    def productos_a_listado(self, df):
        """ haz un listado de productos a partir de un string separado por comas """
        print ('** ejecutando...', str(datetime.now()),  'productos_a_listado')

        try:

            unicos = df.Productos.unique()
            listado_productos = []
            for prod in unicos:
                p = str(prod)
                pord_split = p.split(',')
                for i in pord_split:
                    listado_productos.append(i.strip())

            return listado_productos
        # ----------------------------------------------------
        except Exception as e:
            print('optimiza.py - def productos_a_listado', e)
            self.errores(e, '*** error *** def productos_a_listado *** except Exception ***')
            return False

    def ventana_destino(self, df):
        """ genera un diccionario los rangos de fecha y hora para entregar """
        print ('** ejecutando...', str(datetime.now()),  'ventana_destino')
        try:

            destino_ventanas = {}
            # ---------------------------------------
            diff = df.VentanaFechaFinPedido.iloc[0] - df.VentanaFechaInicioPedido.iloc[0]
            # prepara el diccionario del destino
            delta = timedelta(days=1)
            for i in range(0, diff.days + 1):
                dia = df.VentanaFechaInicioPedido.iloc[0] + i * delta
                suma_dia_y_hora = datetime.combine(dia.date(), df.VentanaHoraFinPedido.iloc[0].time())
                destino_ventanas[dia.date()] = suma_dia_y_hora

            return destino_ventanas
        # ----------------------------------------------------
        except Exception as e:
            print('optimiza.py - def ventana_destino', e)
            self.errores(e, '*** error *** def ventana_destino *** except Exception ***')
            return False

    def ventana_viaje(self, df):
        """ genera un diccionario los rangos de fecha y hora que trae el VIAJE actual """
        print ('** ejecutando...', str(datetime.now()),  'ventana_viaje')
        try:

            # ---------------------------------------
            # me traigo VENTANAS del VIAJE
            viaje_ventanas = {}
            for index, row in df.iterrows():
                viaje_ventanas[row['FechaSalidaPedido'].date()] = [row['FechaSalidaPedido'].time(), roui.Tirada]

            return viaje_ventanas
        # ----------------------------------------------------
        except Exception as e:
            print('optimiza.py - def ventana_viaje', e)
            self.errores(e, '*** error *** def ventana_viaje *** except Exception ***')
            return False

    def calcula_ocupacion(self, df):
        print ('** ejecutando...', str(datetime.now()),  'calcula_ocupacion')
        try:
            # ----------------------------------------------------
            # todos los elementos del tablero numerico
            v_sum = df.groupby(df.index).sum()
            v_max = df.groupby(df.index).max()

            ocupacion_volumen = v_sum.Volumen / v_max.VolumenMax * 100
            ocupacion_peso = v_sum.Peso / v_max.PesoMax * 100

            maximos = pd.concat([v_sum.VolumenMax, v_sum.PesoMax, v_sum.ValorMax, ocupacion_volumen], axis=1)
            totales = pd.concat([v_sum.Volumen, v_sum.Peso, v_sum.Valor, ocupacion_volumen, ocupacion_peso], axis=1)

            dif_vol = v_sum.VolumenMax - v_sum.Volumen
            dif_peso = v_sum.PesoMax - v_sum.Peso
            dif_valor = v_sum.ValorMax - v_sum.Valor

            diferencia = pd.concat([v_sum.VolumenMax, v_sum.Volumen, dif_vol,
                                    v_sum.PesoMax, v_sum.Peso, dif_peso,
                                    v_sum.ValorMax, v_sum.Valor, dif_valor,
                                    ocupacion_volumen, ocupacion_peso], axis=1)

            # ----------------------------------------------------


        # ----------------------------------------------------
        except Exception as e:
            print('optimiza.py - def calcula_ocupacion', e)
            self.errores(e, '*** error *** def calcula_ocupacion *** except Exception ***')
            return False

    def recalcula_viaje(self, df):
        """ recalcula Vol, Peso, Valor, Ocupacion Vol Ocupacion, Peso del viaje """
        print ('** ejecutando...', str(datetime.now()),  'recalcula_viaje')
        try:

            #===============================================
            sumas = df.groupby(df.Viaje).sum(numeric_only=True)
            maximos = df.groupby(df.Viaje).max(numeric_only=True)
            #===============================================
            ocu_vol = round(sumas.Volumen / maximos.VolumenMax * 100)
            ocu_peso = round(sumas.Peso / maximos.PesoMax * 100)
            #===============================================

            return ocu_vol, ocu_peso
       # ----------------------------------------------------
        except Exception as e:
            print('optimiza.py - def recalcula_viaje', e)
            self.errores(e, '*** error *** def recalcula_viaje *** except Exception ***')
            return False




    def tablero(self, df):
        """
        Regresa:
        Bloque 1 Plan
        Viajes	Destinos 	Pedidos	Piezas	Volumen (m3) 	Peso (Kg) 	Valor ($)	Km	CO2 (Ton)	Tiempo (Hrs)
        Bloque 2: $
        $ Total	$/Pza	$/m3	$/Kg	$/Valor	$/Km	$/Co2	$/Hr	$/Viaje	$/ m3-kg-km
        Bloque 3: $ / Viaje
        Pzas / Viaje	m3 / Viaje	Kg / Viaje	Valor / Viaje	Km / Viaje	Co2 / Viaje	Hr / Viaje	Ocupacion Volumen / Viaje	Ocupacion Peso / Viaje
        Bloque 4: Restricciones
        Ventanas de Entrega / Viaje	Volumen / Viaje	Peso / Viaje	Valor / Viaje	Acceso / Viaje

        NO debe estar indexado!!!!!
        """
        print ('** ejecutando...', str(datetime.now()),  'tablero')
        try:
            # valida datos
            df[['Volumen', 'Peso', 'Valor', 'Piezas','TiemDescarga']] = df[['Volumen', 'Peso', 'Valor', 'Piezas', 'TiemDescarga']].apply(pd.to_numeric)
            df[['Tirada', 'VolumenMax', 'PesoMax', 'ValorMax']] = df[['Tirada', 'VolumenMax', 'PesoMax', 'ValorMax']].apply(pd.to_numeric)
            df[['RestriccionVolumen', 'OcupacionVolumen', 'OcupacionPeso', 'Km', 'Co2', 'CostoTotal']] = \
            df[['RestriccionVolumen', 'OcupacionVolumen', 'OcupacionPeso', 'Km', 'Co2', 'CostoTotal']].apply(pd.to_numeric)

            df['FechaSalida'] = pd.to_datetime(df['FechaSalida'], dayfirst=True)
            df['FechaRetorno'] = pd.to_datetime(df['FechaRetorno'], dayfirst=True)
            df['FechaEntregaPedido'] = pd.to_datetime(df['FechaEntregaPedido'], dayfirst=True)
            df['FechaSalidaPedido'] = pd.to_datetime(df['FechaSalidaPedido'], dayfirst=True)
            df['VentanaFechaInicioPedido'] = pd.to_datetime(df['VentanaFechaInicioPedido'], dayfirst=True)
            df['VentanaFechaFinPedido'] = pd.to_datetime(df['VentanaFechaFinPedido'], dayfirst=True)
            df['VentanaHoraInicioPedido'] = pd.to_datetime(df['VentanaHoraInicioPedido'], dayfirst=True)
            df['VentanaHoraFinPedido'] = pd.to_datetime(df['VentanaHoraFinPedido'], dayfirst=True)

            df['FechaEntregaTR2'] = pd.to_datetime(df['FechaEntregaTR2'], dayfirst=True)
            df['FechaEjecucionOpti'] = pd.to_datetime(df['FechaEjecucionOpti'], dayfirst=True)
            #---------------------------------------------
            x = {}
            viajes_max = df.groupby(df.Viaje).max()
            viajes_suma = viajes_max.sum(numeric_only=True)

            pedidos_suma = df.sum(numeric_only=True)
            #---------------------------------------------

            #===============================================
            # seccion total planes
            x['Total Viajes'] = len(df.Viaje.unique())
            x['Total Destinos'] = len(df.DestinoTR1.unique())
            x['Total Pedidos'] = len(df.Pedido.unique())

            x['Total Piezas'] = round(pedidos_suma.Piezas,0)
            x['Total m3'] = round(pedidos_suma.Volumen, 1)
            x['Total Kg'] = round(pedidos_suma.Peso, 0)
            x['Total Valor'] = round(pedidos_suma.Valor, 0)

            x['Total Co2'] = round(viajes_suma.Co2.sum(), 1)
            x['Total Km'] = round(viajes_suma.Km.sum(), 0)
            #---------------------------------------------
            # calcula el tiempo
            dif = df.FechaRetorno.iloc[0] - df.FechaSalida.iloc[0]
            tiempo = dif.days * 24 + dif.seconds / 3600
            x['Total Tiempo'] = round(tiempo, 1)
            #===============================================

            #===============================================
            # seccion $
            x['Costo Total'] = viajes_suma.CostoTotal
            if not x['Total Piezas'] == 0:
                x['Costo Pieza'] = round((viajes_suma.CostoTotal / x['Total Piezas']), 2)
            else:
                x['Costo Pieza'] = 0
            if not x['Total m3'] == 0:
                x['Costo Volumen'] = round((viajes_suma.CostoTotal / x['Total m3']), 2)
            else:
                x['Costo Volumen'] = 0
            if not x['Total Kg'] == 0:
                x['Costo Peso'] = round((viajes_suma.CostoTotal / x['Total Kg']), 2)
            else:
                x['Costo Peso'] = 0
            if not x['Total Valor'] == 0:
                x['Costo Valor'] = round((viajes_suma.CostoTotal / x['Total Valor']), 2)
            else:
                x['Costo Valor'] = 0
            if not x['Total Km'] == 0:
                x['Costo Km'] = round((viajes_suma.CostoTotal / x['Total Km']), 2)
            else:
                x['Costo Km'] = 0
            if not x['Total Co2'] == 0:
                x['Costo Co2'] =round((viajes_suma.CostoTotal / x['Total Co2']), 2)
            else:
                x['Costo Co2'] = 0
            if not x['Total Tiempo'] == 0:
                x['Costo Hr'] =round((viajes_suma.CostoTotal / x['Total Tiempo']), 2)
            else:
                x['Costo Hr'] = 0
            if not x['Total Viajes'] == 0:
                x['Costo Viaje'] = round((viajes_suma.CostoTotal / x['Total Viajes']), 2)
            else:
                x['Costo Viaje'] = 0
            try:
                x['Costo M3 Kg Km'] = round((viajes_suma.CostoTotal / (x['Costo Volumen'] + x['Costo Peso'] + x['Costo Km']) / 3 ), 2)
            except:
                x['Costo M3 Kg Km'] = 0
            #===============================================

            # ===============================================
            # seccion  x Viaje
            x['Piezas/Viaje'] = round(x['Total Piezas'] / x['Total Viajes'])
            x['m3/Viaje'] = round(x['Total m3'] / x['Total Viajes'])
            x['Kg/Viaje'] = round(x['Total Kg'] / x['Total Viajes'])
            x['Valor/Viaje'] = round(x['Total Valor'] / x['Total Viajes'])
            x['Km/Viaje'] = round(x['Total Km'] / x['Total Viajes'])
            x['Co2/Viaje'] = round(x['Total Co2'] / x['Total Viajes'])
            x['Hr/Viaje'] = round((x['Total Tiempo'] / x['Total Viajes']), 0)
            x['OCUPACIONVOLUMEN/Viaje'] = round(viajes_suma.OcupacionVolumen / x['Total Viajes'])
            x['OCUPACIONPESO/Viaje'] = round(viajes_suma.OcupacionPeso / x['Total Viajes'])
            # ===============================================

            # ===============================================
            # seccion  Restricciones
            #---------------------------------------------
            max = df.groupby(df.Viaje).max()
            sum = df.groupby(df.Viaje).sum()
            #---------------------------------------------

            #---------------------------------------------
            vent = max.FechaSalidaPedido.dt.time > max.VentanaHoraFinPedido.dt.time
            x['No Respeto Ventana'] = np.sum(vent.tolist())

            vol = sum.Volumen > max.VolumenMax
            x['No Respeto Volumen'] = np.sum(vol.tolist())

            pes = sum.Peso > max.PesoMax
            x['No Respeto Peso'] = np.sum(pes.tolist())

            val = sum.Valor > max.ValorMax
            x['No Respeto Valor'] = np.sum(val.tolist())

            acc = max.VolumenMax > max.RestriccionVolumen
            x['No Respeto Acceso'] = np.sum(acc.tolist())
           # ===============================================

            return x
        # ----------------------------------------------------
        except Exception as e:
            print('optimiza.py - def tablero', e)
            self.errores(e, '*** error *** def tablero *** except Exception ***')
            return False

    def optimiza_cercanos(self, w):
        """ trata de juntar 2 DESTINOS cercanos en un mismo viaje """

        print ('** ejecutando...', str(datetime.now()),  'optimiza_cercanos')
        try:
            # ----------------------------------------------------
            self.rutina_inicial(w)
            # ----------------------------------------------------

            # ----------------------------------------------------
            self.dfOptimizado = None
            df = self.R.copy()
            #----------------------------------------------------

            #----------------------------------------------------
            self.b('***************************************************************************************************','')
            self.b('INICIA OPTIMIZACION CERCANOS', datetime.now())
            x = 'PARAMETROS: %VIAJE VACIO >= {:0,.0f}%; DISTANCIA CERCANA:{:0,.0f}m; Vol:{:0,.0f}%; Peso:{:0,.0f}%, Valor:{:0,.0f}%'.format(ui.vacios, ui.cercania, ui.volumen, ui.peso, ui.valor)
            y = 'Ignora Acceso:{:0,.0f}; Ignora Mat.Compatibilidad:{:0,.0f}; Aumenta Ventana: {:0,.0f} mins'.format(ui.ignoraAcceso, ui.ignoraMatriz, ui.ventana)

            self.b(x, y)
            self.b('***************************************************************************************************','')
            #----------------------------------------------------

            # =============================================================================================================
            # recorre todos los PEDIDOS
            # =============================================================================================================
            cuenta = 0
            for index, pareja1 in df.iterrows():
                #---------------------------------------------------------------------------------
                #un contador para informar como va
                cuenta += 1
                if (cuenta % 10) == 0:
                    print ('cuenta:', cuenta)
                #---------------------------------------------------------------------------------
                self.b('-----------------------------------------------------------------------------------------------', '')
                self.b('{}. Destino {} en viaje {}'.format(cuenta, pareja1.DestinoTR1, str(pareja1.Viaje)), '')
                viaje_pareja1 = pareja1.Viaje
                # busca en arcos
                #---------------------------------------------------------------------------------
                # trae las DISTANCIAS
                distancias = self.A.loc[(self.A.ORIGEN == pareja1.DestinoTR1) & (self.A.DISTANCIA > 0),['DESTINO', 'DISTANCIA', 'TIEMPO']].sort_values(by='DISTANCIA')
                distancias = distancias.set_index(['DESTINO'])

                if 'DISTANCIA' in df.columns:
                    df = df.drop(['DISTANCIA', 'TIEMPO'], axis=1)
                # copia la col DISTANCIA a df
                df = df.join(distancias, on=['DestinoTR1'], how='left')
                #---------------------------------------------------------------------------------

                # elimina distancias mas grandes que dist_cercana
                distancias = distancias.loc[distancias.DISTANCIA <= ui.cercania]

                if not distancias.empty:
                    self.b('Destinos cercanos', distancias.index.values)
                    # =============================================================================================================
                    # recorre todos los destinos cercanos encontrados en ARCOS
                    # =============================================================================================================
                    cont = 0
                    reubique = False
                    for index, destino_cercano_arcos in distancias.iterrows():

                        # salte del loop si reubicaste
                        if reubique == True:
                            break

                        cont += 1
                        self.b('  ----------------------------------------------------------------------------', '')
                        x = destino_cercano_arcos.name + ' [a {:0,.0f} m / tiempo Google: {:0,.0f} min]'.format(destino_cercano_arcos.DISTANCIA, destino_cercano_arcos.TIEMPO/60)
                        self.b('  {}. Tratando de reubicar {} ->>  {}'.format(str(cont), pareja1.DestinoTR1, x), '')

                        #busca en df a ver si trae pedidos
                        pareja2 = df.loc[df.DestinoTR1 == destino_cercano_arcos.name].copy()

                        if not pareja2.empty:

                            for ind, par2 in pareja2.iterrows():
                                self.b('  - - - - - - - - - - - - - - - - - - - - - - - - -', '')
                                self.b('  Pedido {} en viaje {}'.format(par2.Pedido, par2.Viaje), '')

                                # saca del primer PEDIDO
                                destino_pareja2 = par2.DestinoTR1
                                viaje_pareja2 = par2.Viaje

                                if (viaje_pareja2 != viaje_pareja1):
                                    # lo encontre y lo voy a intentar meter al viaje de la pareja1
                                    # ***************************************************
                                    self.b('  Viaje ', viaje_pareja2)

                                    # pedidos_del_viaje
                                    pedidos_del_viaje = df.loc[df.Viaje == pareja1.Viaje]

                                    # cabe, verifica sus 6 restricciones: acceso, vol, peso, $, mat y ventanas? --------------------------------------------------------
                                    if (self.restricc_acceso(ui, df, viaje_pareja2, destino_pareja2, pareja1.Viaje) == True):
                                        if self.restricc_volumen(df, viaje_pareja2, destino_pareja2, pareja1.Viaje) == True:
                                            if self.restricc_peso(df, viaje_pareja2, destino_pareja2, pareja1.Viaje) == True:
                                                if self.restricc_valor(df, viaje_pareja2, destino_pareja2, pareja1.Viaje) == True:
                                                    if (self.restricc_matriz_comp(ui, df, viaje_pareja2, destino_pareja2, pareja1.Viaje) == True):
                                                        bCumple, Fecha, Tirada = self.restricc_ventanas(df, pareja2, pedidos_del_viaje, pareja1.Viaje)
                                                        if bCumple == True:
                                                            # ------------------------------------------------------------------------
                                                            # cambia los datos del NUEVO VIAJE
                                                            df.loc[(df.Viaje == viaje_pareja2) & (df.DestinoTR1 == destino_pareja2), 'Tirada'] = Tirada
                                                            df.loc[(df.Viaje == viaje_pareja2) & (df.DestinoTR1 == destino_pareja2), 'TipoVehiculo'] = pareja1.TipoVehiculo
                                                            df.loc[(df.Viaje == viaje_pareja2) & (df.DestinoTR1 == destino_pareja2), 'VolumenMax'] = pareja1.VolumenMax
                                                            df.loc[(df.Viaje == viaje_pareja2) & (df.DestinoTR1 == destino_pareja2), 'PesoMax'] = pareja1.PesoMax
                                                            df.loc[(df.Viaje == viaje_pareja2) & (df.DestinoTR1 == destino_pareja2), 'ValorMax'] = pareja1.ValorMax
                                                            if not Fecha == None:
                                                                df.loc[(df.Viaje == viaje_pareja2) & (df.DestinoTR1 == destino_pareja2), 'FechaEntregaPedido'] = Fecha
                                                                # ------------------------------------------------------------------------
                                                                # SUMA el tiempo de servicio a la hora de entrega. Trae tiempo de servicio en TIME y transformalo a mins
                                                                tiemDescarga = df.loc[(df.Viaje == viaje_pareja2) & (df.DestinoTR1 == destino_pareja2), ['TiemDescarga']]
                                                                # ------------------------------------------------------------------------
                                                                nueva_fecha = Fecha + timedelta(minutes = int(tiemDescarga.max()[0]))
                                                                # metelo a la hora fin de entrega y a la FechaSalidaPedido
                                                                df.loc[(df.Viaje == viaje_pareja2) & (df.DestinoTR1 == destino_pareja2), 'FechaSalidaPedido'] = nueva_fecha
                                                                # ------------------------------------------------------------------------
                                                            if (not df.loc[(df.Viaje == viaje_pareja2) & (df.DestinoTR1 == destino_pareja2), 'ModificadoCercanos'].values.any() == 1):
                                                                self.contBitacora_cambios += 1
                                                                df.loc[(df.Viaje == viaje_pareja2) & (df.DestinoTR1 == destino_pareja2), 'ModificadoCercanos'] = 1
                                                            # ------------------------------------------------------------------------
                                                            # el viaje se reemplaza hasta el FINALLLLLL
                                                            df.loc[(df.Viaje == viaje_pareja2) & (df.DestinoTR1 == destino_pareja2), 'Viaje'] = viaje_pareja1
                                                            # ------------------------------------------------------------------------

                                                            self.b('  >>>>>>>>>>>>>>>>>>>>>>>>>>> REUBIQUE AL NUEVO VIAJE - Número de pedidos...',  str(viaje_pareja1))
                                                            self.b('-----------------------------------------------------------------------------------------------','')

                                                            # ------------------------------------------------------------------------
                                                            # recalcula viaje
                                                            v = df.loc[df.Viaje == viaje_pareja1]
                                                            df.OcupacionVolumen, df.OcupacionPeso = self.recalcula_viaje(v)
                                                            # ------------------------------------------------------------------------
                                                            reubique = True
                                                            break
                                else:
                                    # ***************************************************
                                    self.b('...OK!', 'VA EN MISMO VIAJE!!!')
                        else:
                            # ***************************************************
                            self.b('NO HAY PEDIDOS CERCANOS EN PLAN', '')
                else:
                    # ***************************************************
                    self.b('NO HAY DESTINOS CERCANOS', '')

            #----------------------------------------------------
            df = self.rutina_final(ui, df)
            #----------------------------------------------------

            # ----------------------------------------------------
            print('Guardando Bitacora', str(datetime.now() - self.t))
            self.guarda_ss(self.bitacora, ui.ss, 'BITACORA CERCANOS', borra_wks=True, folder_id=None, nuevo_ss=False, nuevo_wks=False)
            print('Termine Guardar en SS', str(datetime.now() - self.t))
            print('Termine Cercanos', str(datetime.now() - self.t))
            # ----------------------------------------------------

            self.dfOptimizado = df
            return True
        # ----------------------------------------------------
        except Exception as e:
            print('optimiza.py - def optimiza_cercanos', e)
            self.errores(e, 'CERCANOS.PY - def optimiza_cercanos *** except Exception ***')
            return False

# ============================================================================================================

    def optimiza_vacios(self, w):
        """ trata de eliminar los viajes mas vacios, y pasar los pedidos a otros viajes"""
        # recibe data frame  *** con todas las columnas *** de Resultados, Compatibilidad y Arcos

        print ('** ejecutando...', str(datetime.now()),  'optimiza_vacios')
        try:
            # ----------------------------------------------------

            print('Rutina Inicial', str(datetime.now() - self.t))
            self.rutina_inicial(w)
            print('Rutina Inicial...listo', str(datetime.now() - self.t))
            #----------------------------------------------------

            #----------------------------------------------------
            ROriginal = self.R.copy()
            # ----------------------------------------------------
            # inicializa el data frame que vamos a REGRESAR
            self.dfOptimizado = None
            OPTIMIZADO = pd.DataFrame()
            OPTIMIZADO = self.R.copy()
            OPTIMIZADO = OPTIMIZADO[0:0]  # quita todos los renglones

            #----------------------------------------------------
            self.b('***************************************************************************************************','')
            self.b('INICIA OPTIMIZACION VACIOS', datetime.now())
            x = 'PARAMETROS: %VIAJE VACIO >= {:0,.0f}%; DISTANCIA CERCANA:{:0,.0f}m; Vol:{:0,.0f}%; Peso:{:0,.0f}%, Valor:{:0,.0f}%'.format(ui.vacios, ui.cercania, ui.volumen, ui.peso, ui.valor)
            y = 'Ignora Acceso:{:0,.0f}; Ignora Mat.Compatibilidad:{:0,.0f}; Aumenta Ventana: {:0,.0f} mins'.format(ui.ignoraAcceso, ui.ignoraMatriz, ui.ventana)
            self.b(x, y)
            self.b('***************************************************************************************************','')
            #----------------------------------------------------

            # inicializa RESTO
            RESTO = self.R.copy()

            # =============================================================================================================
            # Encuentra VIAJES VACIOS < porcentaje_vacio
            # =============================================================================================================
            v_todos = RESTO.groupby(RESTO.Viaje).max().sort_values('OcupacionVolumen')
            # Solo los menores a porcentaje_vacio
            v_vacios = v_todos.loc[v_todos.OcupacionVolumen < (100 - ui.vacios)]
            # ***************************************************
            self.b('VIAJES VACIOS', v_vacios.index.values)
            # =============================================================================================================
            # Para todos los VIAJES VACIOS < porcentaje_vacio
            # =============================================================================================================
            v_vacio = 0
            cuenta1 = 0
            for v_vacio in v_vacios.index:

                ui.avance = v_vacio / v_vacios.shape[0] * 100
                ui.progress.set(str(ui.avance))

                # s = 'Avance....{} de {} viajes vacíos encontrados ({}%)'.format(v_vacio, v_vacios.shape[0], z)
                # ui.avance.set(s)
                # ui.widget_avance.delete(0, END)
                # ui.widget_avance.insert(0, s)



                cuenta1 += 1
                print('{}. VACIO {} DE {}'.format(cuenta1, v_vacio, v_vacios.index.values))

                # DESTINOS dentro del Viaje
                destinos = RESTO.loc[RESTO.Viaje == v_vacio].groupby('DestinoTR1').max()

                #____________________________________________________
                # quita de RESTO el viaje vacio
                RESTO = RESTO.loc[RESTO.Viaje != v_vacio]
                #____________________________________________________

                # ***************************************************
                self.b('------------------------------------------------------------', '')
                self.b('Viaje Vacio', v_vacio)
                self.b('Destinos del Viaje', destinos.index.values)
                # =============================================================================================================
                # Para todos los DESTINOS en el viaje
                # =============================================================================================================
                cuenta2 = 0
                for destino in destinos.index:
                    cuenta2 += 1
                    # ***************************************************
                    self.b('----------------------------------', '')
                    self.b('{}. Destino {}'.format(cuenta2, destino), '')
                    # los pedidos del destino dentro del VIAJE
                    pedidos_del_destino =self.R.loc[(self.R.Viaje == v_vacio) & (self.R.DestinoTR1 == destino)].copy()

                    # trae las DISTANCIAS relacionadas con este destino y anadelo como columna
                    dist = self.A.loc[(self.A.ORIGEN == destino) & (self.A.DISTANCIA > 0), ['DESTINO', 'DISTANCIA', 'TIEMPO']].sort_values(by='DISTANCIA').copy()
                    distancias = dist.set_index(['DESTINO'])

                    # si encontraste distancias cercanas
                    if not distancias.empty:
                        if 'DISTANCIA' in RESTO.columns:
                            RESTO = RESTO.drop(['DISTANCIA', 'TIEMPO'], axis=1)
                        # copia la col DISTANCIA a RESTO
                        RESTO = RESTO.join(distancias, on=['DestinoTR1'], how='left')

                        # consigue los posibles viajes a meter, elimina los lejanos...  < dist_cercana
                        viajes_posibles = RESTO.groupby('Viaje').min().sort_values('DISTANCIA')
                        viajes_posibles = viajes_posibles.loc[viajes_posibles.DISTANCIA <= ui.cercania]

                        if not viajes_posibles.empty:
                            self.b('Posibles viajes cercanos < (' + str(ui.cercania) + ' m)', viajes_posibles.index.values)

                            # trata de meter el "destino" en los siguientes VIAJES, tomo el + cercano primero
                            cuenta3 = 0
                            for viaje_quiero_meter in viajes_posibles.index:
                                cuenta3 += 1
                                # ***************************************************
                                y = RESTO.loc[RESTO.Viaje == viaje_quiero_meter, ['DISTANCIA', 'TIEMPO']].sort_values(by=['DISTANCIA'])
                                y = y.iloc[0]
                                x = '[a {:0,.0f} m / tiempo Google: {:0,.0f} min]'.format(y.DISTANCIA, y.TIEMPO / 60)
                                self.b('{}. Viaje Cercano {}'.format(cuenta3, str(viaje_quiero_meter)), x)

                                pedidos_del_viaje = self.R.loc[self.R.Viaje == viaje_quiero_meter].copy()

                                # cabe, verifica sus 6 restricciones: acceso, vol, peso, $, mat y ventanas? --------------------------------------------------------
                                if (self.restricc_acceso(ui, self.R, v_vacio, destino, viaje_quiero_meter) == True):
                                    if self.restricc_volumen(self.R, v_vacio, destino, viaje_quiero_meter) == True:
                                        if self.restricc_peso(self.R, v_vacio, destino, viaje_quiero_meter) == True:
                                            if self.restricc_valor(self.R, v_vacio, destino,viaje_quiero_meter) == True:
                                                if (self.restricc_matriz_comp(ui, self.R, v_vacio, destino, viaje_quiero_meter) == True):
                                                    bCumple, Fecha, Tirada = self.restricc_ventanas(RESTO,
                                                                                                          pedidos_del_destino,
                                                                                                          pedidos_del_viaje,
                                                                                                          viaje_quiero_meter)
                                                    if bCumple == True:
                                                        # ------------------------------------------------------------------------
                                                        # cambia los datos del NUEVO VIAJE
                                                        pedidos_del_destino.Tirada = Tirada
                                                        pedidos_del_destino.Viaje = viaje_quiero_meter
                                                        pedidos_del_destino.TipoVehiculo = pedidos_del_viaje.iloc[0].TipoVehiculo
                                                        pedidos_del_destino.VolumenMax = pedidos_del_viaje.iloc[0].VolumenMax
                                                        pedidos_del_destino.PesoMax = pedidos_del_viaje.iloc[0].PesoMax
                                                        pedidos_del_destino.ValorMax = pedidos_del_viaje.iloc[0].ValorMax

                                                        if not Fecha == None:
                                                            pedidos_del_destino.FechaEntregaPedido = Fecha

                                                            # ------------------------------------------------------------------------
                                                            # SUMA el tiempo de servicio a la hora de entrega. Trae tiempo de servicio en TIME y transformalo a mins
                                                            tiemDescarga = pedidos_del_destino.iloc[0].TiemDescarga
                                                            # ------------------------------------------------------------------------
                                                            nueva_fecha = Fecha + timedelta(minutes=int(tiemDescarga))
                                                            # anadelo a la hora fin de entrega y a la FechaSalidaPedido
                                                            pedidos_del_destino.FechaSalidaPedido = nueva_fecha
                                                            # ------------------------------------------------------------------------

                                                        if (not pedidos_del_destino['ModificadoVacios'].values.any() == 1):
                                                            self.contBitacora_cambios += 1
                                                            pedidos_del_destino['ModificadoVacios'] = 1

                                                        self.b('>>>>>>>>>>>>>>>>>>>>>>>>>>> REUBIQUE AL NUEVO VIAJE - Número de pedidos...',
                                                               str(pedidos_del_destino.Viaje.values))
                                                        self.b('---------------------------------------------------------------------------------------------', '')


                                                        break

                        else: #if not viajes_posibles.empty:
                            self.b('No Hay Distancias Cercanas en el PLAN', ui.cercania)
                    else: #if not distancias.empty:
                         self.b('No Hay Destinos Cercanas en los ARCOS', ui.cercania)

                    # copio
                    OPTIMIZADO = pd.concat([OPTIMIZADO, pedidos_del_destino])

            # # ----------------------------------------------------
            # # acabe con posibles viajes
            # # recalcula viaje
            # viajes_recalculado = self.recalcula_viaje(pedidos_del_destino)
            # # copio el Destino a REGRESO, sin cambios
            # OPTIMIZADO = pd.concat([OPTIMIZADO, viajes_recalculado])
            # # ----------------------------------------------------

            # ----------------------------------------------------
            # quita de RESTO el viaje vacio
            RESTO = RESTO.loc[RESTO.Viaje != v_vacio]
            # quita columnas
            if 'DISTANCIA' in RESTO.columns:
                RESTO = RESTO.drop(['DISTANCIA', 'TIEMPO'], axis=1)

            # copio los que NO estaban vacios
            OPTIMIZADO = pd.concat([RESTO, OPTIMIZADO])
            # ----------------------------------------------------

            #----------------------------------------------------
            OPTIMIZADO = self.rutina_final(ui, OPTIMIZADO)
            #----------------------------------------------------

            #----------------------------------------------------
            print ('Guardando Bitacora', str(datetime.now()- self.t))
            self.guarda_ss(self.bitacora, ui.ss, 'BITACORA VACIOS', borra_wks=True, folder_id=None, nuevo_ss=False, nuevo_wks=False)
            print ('Termine Guardar en SS', str(datetime.now()- self.t))

            #----------------------------------------------------
            print('Termine Vacios', str(datetime.now() - self.t))

            self.dfOptimizado = OPTIMIZADO
            return True
        # ----------------------------------------------------
        except Exception as e:
            print('optimiza.py - def optimiza_vacios', e)
            self.errores(e, 'optimiza.py - def optimiza_vacios *** except Exception ***')
            return False



