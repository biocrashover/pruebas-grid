# para traducir...      pyuic5 -x test.ui > test.py
#                       pyuic5 recursos.qrc -o recursos_rc.py

import sys
from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtGui import QIcon
import sys
import os

import PyQt5



#Qt windows creadas en Qt Creator
from Qt.arms import *


# ---------------------------------------------------------------------
# bitácora
def b(s1, s2):
    config.contador += 1
    if len(str(s2).strip()) > 0:
        s = str(s1).strip() + '... [' + str(s2).strip() + ']'
    else:
        s = str(s1).strip()
    config.bit.loc[config.contador] = [s]
# ---------------------------------------------------------------------


# ---------------------------------------------------------------------
# Clases con acciones
from Qt.arms_signals import *

# ---------------------------------------------------------------------





# ---------------------------------------------------------------------
if __name__ == "__main__":
    try:
        app = QtWidgets.QApplication(sys.argv)
        ARMS = QtWidgets.QMainWindow()
        ui = Ui_ARMS()
        ui.setupUi(ARMS)

        # ---------------------------------------------------------------------
        #instancias de las clases que contienen ACCIONES del Qt
        a_btn = AccionesBotones(ui)
        # ---------------------------------------------------------------------

        # ---------------------------------------------------------------------
        # has las CONNECT de cada widget

        # Generador de Rutas / 3. Optimiza Automatico
        #ui.btnAbreSSResultados.clicked.connect(a_btn.btnAbreSSResultados)
        # ---------------------------------------------------------------------

        # OBTENER SS DE VEHICULOS
        #ui.pushButton.clicked.connect(a_btn.iniciarSesion)

        # ---------------------------------------------------------------------
        ARMS.show()
        sys.exit(app.exec_())
    # ----------------------------------------------------
    except Exception as e:
        file_name = os.path.basename(sys.argv[0])
        this_function_name = sys._getframe().f_code.co_name
        error = '{} - {} - {}'.format(file_name,this_function_name,str(e))
        print(error)

# ---------------------------------------------------------------------

