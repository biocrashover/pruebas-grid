# para traducir...      pyuic5 -x test.ui > test.py
#                       pyuic5 recursos.qrc -o recursos_rc.py

import sys
from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtGui import QIcon
import sys
import os

# ---------------------------------------------------------------------
#para abrir archivos ss y s3
import pygsheets
# ---------------------------------------------------------------------

# ---------------------------------------------------------------------
#Qt windows creadas en Qt Creator
from Qt.arms import *
# ---------------------------------------------------------------------

# ---------------------------------------------------------------------
from Pruebas.pruebasMiguelModulo import *
# ---------------------------------------------------------------------


# ---------------------------------------------------------------------
# bitácora
def b(s1, s2):
    config.contador += 1
    if len(str(s2).strip()) > 0:
        s = str(s1).strip() + '... [' + str(s2).strip() + ']'
    else:
        s = str(s1).strip()
    config.bit.loc[config.contador] = [s]
# ---------------------------------------------------------------------


# ---------------------------------------------------------------------
# Clases con acciones
from Qt.arms_signals import *

# ---------------------------------------------------------------------

def fnPreparaTodoMia():
    empresa = 'SILODISA'
    cedi = 'CENADI'
    nombre_arcos = '../Datos/ArcosSilodisa.txt'
    # --------------------------------------------------------------------
    nombre_ss = '1OgSKfUSUN0k_LvYV-BKrQNtjy6sHBzmKXp63x7_CfBY'

    porc_vacio = 90  # o menos
    dist_cercana = 20000  # ...viene en metros
    vol = 110
    peso = 110
    valor = 100
    aumenta_mins_a_ventana = 83
    ignora_mat_comp = False
    ignora_acceso = False
    
    # abre R, C y arcos
    # ---------------------------------------------------
    # abre SS
    config.dfResultados = carga_ss(nombre_ss, 'RESULTADOS', tipo_nombre_ss='key')
    config.dfCompatibilidad = carga_ss(nombre_ss, 'COMPATIBILIDAD', abierto=True)
    # ---------------------------------------------------
    # abre arcos
    config.Arcos = pd.read_csv(nombre_arcos, sep='\t',
                         names=['ORIGEN', 'DESTINO', 'DISTANCIA', 'TIEMPO', 'LATITUD_DESTINO_A',
                                'LONGITUD_DESTINO_A', 'LATITUD_DESTINO_B', 'LONGITUD_DESTINO_B',
                                'FECHA_CALCULO'])
    # ---------------------------------------------------



# en config puse global config.mainWindow
# ********************* EN EL MAINWINDOW DE EDUARDO *********************
def arms_signals(ui):
    ui.pushButtonOptimiza.clicked.connect(optimizaAutomatico)
# ***********************************************************************

# ================================== optimizaAutomatico.py =======================================





def carga_ss(nombre_ss, nombre_wks, tipo_nombre_ss='key', abierto=False):
    # corre cronometro
    print('** ejecutando...', str(datetime.now()), 'carga_ss')
    start_time = datetime.now()

    try:
        # TODO: pide autorizacion
        if not abierto:

            config.gc = pygsheets.authorize(outh_file='../ImportarDatos/client_secret.json')
            # TODO: abre el ss
            if tipo_nombre_ss == 'key':
                config.sheet = config.gc.open_by_key(nombre_ss)
            elif tipo_nombre_ss == 'url':
                config.sheet = config.gc.open_by_url(nombre_ss)
            else:
                config.sheet = config.gc.open(nombre_ss)
        # TODO: selecciona el wks
        wks = config.sheet.worksheet_by_title(nombre_wks)
        df = wks.get_as_df()
        # TODO: borra los 4 primeros renglones. Siempre en todos los SS de ARMS
        df = df.drop(df.index[0:3])


        # guarda como json
        # df.to_json('Datos/resultados.json')
        #

    # ----------------------------------------------------
    except Exception as e:
        file_name = os.path.basename(sys.argv[0])
        this_function_name = sys._getframe().f_code.co_name
        error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
        print(error)

    # para cronometro
    duracion = datetime.now() - start_time

    return df


def guarda_ss(df, nombre_ss, nombre_wks, tipo_nombre_ss='key', borra_wks=False, folder_id=None, nuevo_ss=False,
              nuevo_wks=False):
    print('** ejecutando...', str(datetime.now()), 'guarda_ss')
    # corre cronometro
    start_time = datetime.now()
    try:
        # checa si hubo error...si viene df = False, si lo hubo
        # TODO: pide autorizacion
        config.gc = pygsheets.authorize(outh_file='clases/client_secret.json')
        # TODO: es un nuevo ss?
        if nuevo_ss == True:
            sheet = config.gc.create(nombre_ss, parent_id=folder_id)
            wks = sheet.worksheet('index', 0)
        else:
            # TODO: abre el ss
            if tipo_nombre_ss == 'key':
                config.sheet = config.gc.open_by_key(nombre_ss)
            elif tipo_nombre_ss == 'url':
                config.sheet = config.gc.open_by_url(nombre_ss)
            else:
                config.sheet = config.gc.open(nombre_ss)
            # TODO: genera un nuevo wks si fue pedido
            if nuevo_wks == True:
                config.sheet.add_worksheet(nombre_wks)
                wks = config.sheet.worksheet_by_title(nombre_wks)
            else:
                wks = config.sheet.worksheet_by_title(nombre_wks)
        # TODO: mete el df al wks

        # borr wks
        df2 = df.astype(str)
        if (borra_wks == True):
            wks.clear(start='A5', end='BQ1000')
            wks.set_dataframe(df2, (5, 1), fit=True, copy_head=False)
        else:
            wks.set_dataframe(df2, (5, 1), fit=True, copy_head=False)
    # ----------------------------------------------------
    except Exception as e:
        file_name = os.path.basename(sys.argv[0])
        this_function_name = sys._getframe().f_code.co_name
        error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
        print(error)

    # para cronometro
    duracion = datetime.now() - start_time

    return True




# =======================================================================================
# =======================================================================================

if __name__ == "__main__":
    try:
        # ---------------------------------------------------------------------
        app = QtWidgets.QApplication(sys.argv)
        ARMS = QtWidgets.QMainWindow()
        config.mainWindow = Ui_ARMS()
        config.mainWindow.setupUi(ARMS)
        # ---------------------------------------------------------------------

        #rutina mia para abrir las cosas que necesito
        #fnPreparaTodoMia()

        # ********************* EN EL MAINWINDOW DE EDUARDO *********************
        # Optimiza Automatico
        arms_signals(config.mainWindow)
        # ***********************************************************************


        # ---------------------------------------------------------------------
        ARMS.show()
        sys.exit(app.exec_())
    # ----------------------------------------------------
    except Exception as e:
        file_name = os.path.basename(sys.argv[0])
        this_function_name = sys._getframe().f_code.co_name
        error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
        print(error)

# =======================================================================================
# =======================================================================================

