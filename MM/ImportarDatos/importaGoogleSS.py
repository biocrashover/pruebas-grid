import config
import pygsheets
from datetime import datetime
import sys
import os

def cargaResultados(nombre_ss, nombre_wks, tipo_nombre_ss='key', abierto=False):
    print('** ejecutando...', str(datetime.now()), 'carga_ss')
    start_time = datetime.now()
    # ----------------------------------------------------
    try:
        # TODO: pide autorizacion
        if not abierto:
            config.gc = pygsheets.authorize(outh_file='datos/client_secret.json')
            # TODO: abre el ss
            if tipo_nombre_ss == 'key':
                config.sheet = config.gc.open_by_key(nombre_ss)
            elif tipo_nombre_ss == 'url':
                config.sheet = config.gc.open_by_url(nombre_ss)
            else:
                config.sheet = config.gc.open(nombre_ss)
        # TODO: selecciona el wks
        wks = config.sheet.worksheet_by_title(nombre_wks)
        df = wks.get_as_df()
        # TODO: borra los 4 primeros renglones. Siempre en todos los SS de ARMS
        df = df.drop(df.index[0:3])
    # ----------------------------------------------------
    except Exception as e:
        file_name = os.path.basename(sys.argv[0])
        this_function_name = sys._getframe().f_code.co_name
        error = '{} - {} - {}'.format(file_name,this_function_name,str(e))
        return False, error, False
    # para cronometro
    duracion = datetime.now() - start_time
    return True, duracion, df

