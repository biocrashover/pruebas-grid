import pygsheets
from datetime import datetime
import sys
import os

def guardaResultados(self, df, nombre_ss, nombre_wks, tipo_nombre_ss='key', borra_wks=False, folder_id=None, nuevo_ss=False,nuevo_wks=False):
    print('** ejecutando...', str(datetime.now()), 'guarda_ss')
    # corre cronometro
    start_time = datetime.now()
    # ----------------------------------------------------
    try:
        # checa si hubo error...si viene df = False, si lo hubo
        # TODO: pide autorizacion
        self.gc = pygsheets.authorize(outh_file='clases/client_secret.json')
        # TODO: es un nuevo ss?
        if nuevo_ss == True:
            sheet = self.gc.create(nombre_ss, parent_id=folder_id)
            wks = sheet.worksheet('index', 0)
        else:
            # TODO: abre el ss
            if tipo_nombre_ss == 'key':
                self.sheet = self.gc.open_by_key(nombre_ss)
            elif tipo_nombre_ss == 'url':
                self.sheet = self.gc.open_by_url(nombre_ss)
            else:
                self.sheet = self.gc.open(nombre_ss)
            # TODO: genera un nuevo wks si fue pedido
            if nuevo_wks == True:
                self.sheet.add_worksheet(nombre_wks)
                wks = self.sheet.worksheet_by_title(nombre_wks)
            else:
                wks = self.sheet.worksheet_by_title(nombre_wks)
        # TODO: mete el df al wks

        # borr wks
        df2 = df.astype(str)
        if (borra_wks == True):
            wks.clear(start='A5', end='BQ1000')
            wks.set_dataframe(df2, (5, 1), fit=True, copy_head=False)
        else:
            wks.set_dataframe(df2, (5, 1), fit=True, copy_head=False)
    # ----------------------------------------------------
    except Exception as e:
        file_name = os.path.basename(sys.argv[0])
        this_function_name = sys._getframe().f_code.co_name
        error = '{} - {} - {}'.format(file_name,this_function_name,str(e))
        return error, 0, False
    # ----------------------------------------------------
    duracion = datetime.now() - start_time
    return True, duracion, df


